<?php

use core\View;

class HomeController
{
    public function index()
    {
        return View::show('index', 'home');
    }
}