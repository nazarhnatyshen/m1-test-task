<?php

use app\Models\Post;
use core\View;

class PostController extends \app\Controllers\BaseController
{
    public function getAllPosts()
    {
        $posts = Post::getAllPosts();

        echo json_encode($posts);
    }

    public function createPost()
    {
        $post = Post::create($_POST);

        echo json_encode($post);
    }

    /**
     * @param $id
     */
    public function getPost($id)
    {
        $post = Post::find((int)$id);

        return View::show('index', 'post', $post);
    }

    /**
     * @param $id
     */
    public function updatePost($id)
    {
        $post = Post::find((int)$id);

        if (empty($post)) {
            http_response_code(404);
        }

        echo $post->update($_POST);
    }

    /**
     * @param $id
     */
    public function deletePost($id)
    {
        $post = Post::find((int)$id);

        if (empty($post)) {
            http_response_code(404);
        }

        echo $post->delete();
    }
}