<?php

namespace app\Models;

use core\Model;
use core\PdoHelper;

class Post extends Model
{
    /**
     * @return array
     */
    public static function getAllPosts()
    {
        $posts = PdoHelper::selectAll('posts', \PDO::FETCH_ASSOC);

        return [
            'posts' => $posts,
        ];
    }

    /**
     * @param $data
     * @return mixed
     */
    public static function create($data)
    {
        $inserted = PdoHelper::insert('posts', [
            'title' => $data['title'],
            'text' => $data['text'],
            'img' => $data['img'],
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        return $inserted;
    }

    /**
     * @param $id
     * @return null
     */
    public static function find($id)
    {
        $post = PdoHelper::select(
            'SELECT * FROM posts WHERE id = :id',
            [
                ':id' => $id,
            ],
            \PDO::FETCH_CLASS,
            Post::class
        );

        return $post[0] ?? null;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function update($data)
    {
        $post = PdoHelper::update(
            'posts',
            [
                'title' => $data['title'],
                'text' => $data['text'],
                'img' => $data['img'],
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => $this->id,
            ]
        );

        return $post;
    }

    /**
     * @return mixed
     */
    public function delete()
    {
        return PdoHelper::delete('posts', ['id' => $this->id]);
    }
}