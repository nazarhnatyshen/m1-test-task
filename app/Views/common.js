function getPosts() {
    $.ajax({
        url: '/posts',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            $.each(data.posts, function () {
                var newPost = $('#posts > div').first().clone();

                $(newPost).find('.card-title').text(this.title);
                $(newPost).find('.card-text').text(this.text.substring(0, 50) + '...');
                $(newPost).find('.card-img-top').attr('src', this.img);
                $(newPost).find('.card-footer.text-muted').text(this.updated_at);
                $(newPost).find('.post-link').attr('href', '/posts/' + this.id);
                $(newPost).appendTo('#posts');
                $(newPost).show();
            });
        }
    });
}

function createPost() {
    $.ajax({
        url: '/posts',
        type: 'post',
        data: $('#create-post').serialize(),
        success: function () {
            $('#create-post').hide();
        }
    });
}

function editPost(id) {
    $.ajax({
        url: '/posts/edit/' + id,
        type: 'POST',
        data: $('#edit-post-form').serialize(),
        success: function () {
            location.reload();
        }
    });
}

function deletePost(id) {
    $.ajax({
        url: '/posts/delete/' + id,
        type: 'POST',
        data: {},
        success: function () {
            history.go(-1);
        }
    });
}