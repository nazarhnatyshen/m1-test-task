<div class="container">
    <h1>Blog</h1>
    <a class="btn btn-info" role="button" id="add-post">Add post</a>
    <div class="row" style="padding: 20px;">
        <div class="col-sm-4">
            <form id="create-post" method="post">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" id="title" placeholder="Title">
                </div>
                <div class="form-group">
                    <label for="text">Example textarea</label>
                    <textarea class="form-control" name="text" id="text" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <label for="text">Image URL</label>
                    <input type="url" name="img" class="form-control" id="img" placeholder="Image URL">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    <div class="row" id="posts">
        <div class="col-sm-3" style="display: none;">
            <div class="card">
                <img class="card-img-top" src="http://placehold.it/200x300" alt="Card image cap">
                <div class="card-body">
                    <h2 class="card-title">Post Title</h2>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid
                        atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero
                        voluptate voluptatibus possimus, veniam magni quis!</p>
                    <a href="#" class="post-link btn btn-primary">Read More &rarr;</a>
                </div>
                <div class="card-footer text-muted">
                    Posted on January 1, 2017 by
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#create-post').hide();
        getPosts();
        $('#create-post').submit(function (e) {
            e.preventDefault();
            createPost();
        });
        $('#add-post').click(function (e) {
            e.preventDefault();
            $('#create-post').toggle();
        });
    });
</script>