<div class="container">
    <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-12">
            <a class="btn btn-secondary" role="button" onclick="history.go(-1);">Back ←</a>
            <a class="btn btn-warning" role="button" id="edit-post-button">Edit post</a>
            <a class="btn btn-danger" role="button" id="delete-post-button">Delete post</a>
            <div id="edit-post-div" class="row" style="padding: 20px;">
                <div class="col-sm-12">
                    <form id="edit-post-form" method="post">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input value="<?= $from->title ?>" type="text" name="title" class="form-control" id="title"
                                   placeholder="Title">
                        </div>
                        <div class="form-group">
                            <label for="text">Example textarea</label>
                            <textarea class="form-control" name="text" id="text" rows="5"><?= $from->text ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="text">Image URL</label>
                            <input value="<?= $from->img ?>" type="url" name="img" class="form-control" id="img"
                                   placeholder="Image URL">
                        </div>
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </form>
                </div>
            </div>

            <!-- Title -->
            <h1 class="mt-4"><?= $from->title ?></h1>

            <hr>

            <!-- Date/Time -->
            <p><?= $from->updated_at ?></p>

            <hr>

            <!-- Preview Image -->
            <img class="img-fluid rounded" src="<?= $from->img ?>" alt="">

            <hr>

            <!-- Post Content -->
            <p class="lead"><?= $from->text ?></p>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#edit-post-div').hide();

        $('#edit-post-button').click(function () {
            $('#edit-post-div').toggle();
        });

        $('#edit-post-form').submit(function (e) {
            e.preventDefault();
            editPost(<?= $from->id ?>);
        });

        $('#delete-post-button').click(function (e) {
            e.preventDefault();
            deletePost(<?= $from->id ?>);
        });
    });
</script>