<?php

use core\Route;


Route::get('/', 'Home@index');

Route::get('/posts', 'Post@getAllPosts');
Route::get('/posts/{id}', 'Post@getPost');
Route::post('/posts/edit/{id}', 'Post@updatePost');
Route::post('/posts/delete/{id}', 'Post@deletePost');

Route::post('/posts', 'Post@createPost');