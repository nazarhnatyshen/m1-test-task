<?php

namespace core;

use PDO;

class DB
{
    private static $dbData = null;
    private static $link = null;

    private static function getHandler()
    {
        self::$dbData = include DIR_CONFIG . '/database.php';
        if (self::$link) {
            return self::$link;
        }
        self::$link = new PDO(
            self::$dbData['engine'] . ':host=' . self::$dbData['host'] . ';dbname=' . self::$dbData['database'] . '', self::$dbData['user'], self::$dbData['password'],
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            ]
        );

        return self::$link;
    }

    public static function __callStatic($name, $args)
    {
        return call_user_func_array([self::getHandler(), $name], $args);
    }
}