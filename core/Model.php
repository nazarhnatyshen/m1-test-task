<?php

namespace core;

class Model
{

    protected $table;

    /**
     * @param $array
     */
    public function fillFromArray($array)
    {
        foreach ($array as $key => $value) {
            $this->$key = $value;
        }
    }
}