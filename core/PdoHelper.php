<?php

namespace core;

use PDO;

class PdoHelper
{
    /**
     * @param $sql
     * @return mixed
     */
    public static function raw($sql)
    {
        return DB::query($sql);
    }

    /**
     * @param $from
     * @param int $fetchMode
     * @param string $class
     * @return mixed
     */
    public static function selectAll($from, $fetchMode = PDO::FETCH_OBJ, $class = '')
    {
        return self::select('select * from ' . $from, [], $fetchMode, $class);
    }

    /**
     * @param $sql
     * @param array $array
     * @param int $fetchMode
     * @param string $class
     * @return mixed
     */
    public static function select($sql, $array = [], $fetchMode = PDO::FETCH_OBJ, $class = '')
    {
        if (strtolower(substr($sql, 0, 7)) !== 'select ') {
            $sql = 'SELECT ' . $sql;
        }

        $query = DB::prepare($sql);
        foreach ($array as $key => $value) {
            if (is_int($value)) {
                $query->bindValue($key, $value, PDO::PARAM_INT);
            } else {
                $query->bindValue($key, $value);
            }
        }

        $query->execute();

        if ($fetchMode === PDO::FETCH_CLASS) {
            return $query->fetchAll($fetchMode, $class);
        }

        return $query->fetchAll($fetchMode);
    }

    /**
     * @param $table
     * @param $data
     * @return mixed
     */
    public static function insert($table, $data)
    {
        ksort($data);

        $keys = implode(',', array_keys($data));
        $values = ':' . implode(', :', array_keys($data));

        $query = DB::prepare("INSERT INTO $table ($keys) VALUES ($values)");
        foreach ($data as $key => $value) {
            $query->bindValue(":$key", $value);
        }
        $query->execute();

        return DB::lastInsertId();
    }

    /**
     * @param $table
     * @param $data
     * @param $where
     * @return mixed
     */
    public static function update($table, $data, $where)
    {
        ksort($data);

        $fieldParams = null;
        foreach ($data as $key => $value) {
            $fieldParams .= "$key = :$key,";
        }
        $fieldParams = rtrim($fieldParams, ',');
        $whereParams = null;
        $i = 0;

        foreach ($where as $key => $value) {
            if ($i == 0) {
                $whereParams .= "$key = :$key";
            } else {
                $whereParams .= " AND $key = :$key";
            }
            $i++;
        }

        $whereParams = ltrim($whereParams, ' AND ');
        $query = DB::prepare("UPDATE $table SET $fieldParams WHERE $whereParams");

        foreach ($data as $key => $value) {
            $query->bindValue(":$key", $value);
        }

        foreach ($where as $key => $value) {
            $query->bindValue(":$key", $value);
        }

        $query->execute();

        return $query->rowCount();
    }

    /**
     * @param $table
     * @param $where
     * @param int $limit
     * @return mixed
     */
    public static function delete($table, $where, $limit = 1)
    {
        ksort($where);
        $whereParams = null;

        $i = 0;
        foreach ($where as $key => $value) {
            if ($i == 0) {
                $whereParams .= "$key = :$key";
            } else {
                $whereParams .= " AND $key = :$key";
            }
            $i++;
        }

        $whereParams = ltrim($whereParams, ' AND ');
        if (is_numeric($limit)) {
            $limitStr = "LIMIT $limit";
        }

        $stmt = DB::prepare("DELETE FROM $table WHERE $whereParams $limitStr");
        foreach ($where as $key => $value) {
            $stmt->bindValue(":$key", $value);
        }

        $stmt->execute();

        return $stmt->rowCount();
    }
}