<?php

namespace core;


class Route
{
    private static $routers = [];

    public static $allowMethods = ['GET', 'SET', 'PUT', 'PATCH', 'DELETE', 'POST'];

    /**
     * @param $uri
     * @param $callback
     * @param $method
     */
    private static function initRouting($uri, $callback, $method)
    {
        $parsedCallback = explode('@', $callback);
        $controller = array_shift($parsedCallback);
        $action = array_shift($parsedCallback);
        preg_match_all('/\{(.*?)\}/', $uri, $matches);

        $pattern = null;
        if (isset($matches[1])) {
            $replacePatterns = [];
            foreach ($matches[1] as $m) {
                $replacePatterns[] = '/\{' . $m . '\}/';
            }

            $pattern = str_replace('/', '\/', preg_replace($replacePatterns, '(.*)', $uri));
            $pattern = '/^' . $pattern . '$/';
        }

        array_push(self::$routers, [
            'uri' => $uri,
            'controller' => $controller . 'Controller',
            'model' => $controller,
            'action' => $action,
            'method' => strtoupper($method),
            'pattern' => $pattern,
        ]);
    }

    /**
     * @param $name
     * @param $arguments
     */
    public static function __callStatic($name, $arguments)
    {
        if (array_search(strtoupper($name), self::$allowMethods) !== false) {
            if (count($arguments) >= 2) {
                self::initRouting($arguments[0], $arguments[1], $name);
            }
        }
    }

    public static function start()
    {
        $parsedUrl = parse_url($_SERVER['REQUEST_URI']);
        $findedController = false;

        foreach (self::$routers as $router) {
            if (mb_strtoupper($_SERVER['REQUEST_METHOD']) === mb_strtoupper($router['method']) && preg_match($router['pattern'], $parsedUrl['path'], $parameters)) {
                require_once DIR_CONTROLLERS . '/' . $router['controller'] . '.php';
//                require_once DIR_MODELS . '/' . $router['model'] . '.php';

                $object = new $router['controller']();
                $findedController = true;
                if (!empty($parameters)) {
                    array_shift($parameters);
                }

                if (!empty($parameters)) {
                    call_user_func_array([&$object, $router['action']], $parameters);
                } else {
                    call_user_func([&$object, $router['action']]);
                }
            }
        }
        if (!$findedController) {
            echo '404';
        }
    }
}