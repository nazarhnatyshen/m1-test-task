<?php

namespace core;

class View
{
    /**
     * @param $commonView
     * @param $view
     * @param null $from
     */
    public static function show($commonView, $view, $from = null)
    {
        require_once DIR_VIEWS . "/$commonView.php";
    }
}