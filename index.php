<?php

define('DIR_ROOT', __DIR__);
define('DIR_APP', DIR_ROOT . '/app');
define('DIR_CORE', DIR_ROOT . '/core');
define('DIR_CONTROLLERS', DIR_APP . '/Controllers');
define('DIR_MODELS', DIR_APP . '/Models');
define('DIR_VIEWS', DIR_APP . '/Views');
define('DIR_VENDOR', DIR_ROOT . '/vendor');
define('DIR_CONFIG', DIR_ROOT . '/config');


spl_autoload_register(function ($class) {
    $file = str_replace('\\', '/', $class);
    $filePath = $file . '.php';

    if (file_exists($filePath)) {
        require_once $filePath;
    }
});

require_once 'bootstrap.php';